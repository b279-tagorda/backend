// console.log("Hello world!");

// [SECTION] JS synchronous vs Asynchronous
// JS by default is schronous, meaning only one statement is executed at a time.


// TOP -> BOTTOM AND LEFT -> RIGHT
console.log("Hello world!");
// conole.log("Hello Again!");
console.log("goodbye!");

console.log("Hello world!");
// for(let i = 0; i <= 10; i++){
// 	console.log(i);
// }
console.log("Hello Again!");

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
// Syntax
	// fetch('URL')


console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// Syntax
// fetch(URL)
// .then((res) => {})

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise


// "fetch" method will return a promise that resolves to a response object
// "promise" will eventually be "resolved" or "rejected"
(fetch("https://jsonplaceholder.typicode.com/posts"))
.then(res => console.log(res.status));

(fetch("https://jsonplaceholder.typicode.com/posts"))
.then(res => res.json())
.then(json => console.log(json));

// use the "json" method from the response object to convert the data retrieved into JSON format to be used in the application
// print thge converted JSON value from fetch request
// using multiple "then" methods creates "promise chain"


// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates an asynchronous function

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

fetchData();

// [SECTION] Getting a specific post
// Retrieve a specific post following the REST API (retrieve, /posts/:id, GET)

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json())
.then(result => console.log(result));

//[SECTION] Creating a post
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// [SECTION] Updating using PUT method

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		id: 1,
		title: "updated Post",
		body: "Hello again!",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// [SECTION] Updating using PATCH method

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Corrected Post"
	})
})
.then(res => res.json())
.then(json => console.log(json));


// [SECTION] Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});

// [SECTION] Filtering post

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(res => res.json())
.then(json => console.log(json));


// [SECTION] Retrieving nested/related comments to post
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(res => res.json())
.then(json => console.log(json));
