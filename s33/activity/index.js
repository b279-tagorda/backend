// console.log("hello");


// 3
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(result => console.log(result));


// 4 MAP
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let titleMap=title.map((dataToMap) => dataToMap.title)
	console.log(titleMap);

});


// 5 
fetch("https://jsonplaceholder.typicode.com/todos/10")
.then(res => res.json())
.then(result => console.log(result));


// 6

// 7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "created to do list item",
		completed: false,
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// 8

fetch("https://jsonplaceholder.typicode.com/todos/10", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "updated to do list item",
		description: "updated description",
		status: "pending",
		dateCompleted: "today",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/10", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		status: "complete",
		dateCompleted: "tomorrow",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));



fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "DELETE"
});

