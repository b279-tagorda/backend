const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js')
const auth = require("../auth.js")


// route for checking if email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then( resultFromController => res.send(resultFromController));
});


// route for registering a user.`1`
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});



// route for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// route for get profile
// router.post("/details", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)

// 	userController.getProfile(userData.id).then(resultFromController => res.send(resultFromController))
// });


router.post("/:userId/userDetails", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getUserProfile(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

})


// router.put("/:productId/archive", auth.verify, (req, res) => {
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	productController.archieveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
// 									//////req.params nasa URL

// })





// create at order

router.post("/checkOut", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		products : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})



///////////////////////////////============SG

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.setAsAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
									//////req.params nasa URL

})



// Allows us to export the "router" object that will be access in our index.js file.
module.exports = router;