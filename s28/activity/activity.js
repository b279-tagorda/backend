// add one //

db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false

});


// add many//

db.rooms.insertMany([{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
	},
	{
	name: "king",
	accommodates: 2,
	price: 5000,
	description: "A room with a king sized bed perfect for a luxury getaway",
	rooms_available: 20,
	isAvailable: false
	}

	]);

// Find//

db.rooms.find({
	name: "double"
});

// update one//

db.rooms.updateOne(
	{name: "double"},
	{
		$set: {
			rooms_available : 0
		}
	}
);

// delete many //


db.rooms.deleteMany({
	rooms_available: 0
});
