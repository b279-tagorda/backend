// CRUD OPERATIONS

/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [SECTION] Inserting documents (create)
// Insert one document

/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
- By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "phyton"],
	department: "none"

});

// Insert Many
/*
Syntax
- db.collectionName.insertMany([{object a}, {object b}])

*/

db.users.insertMany([
	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
	},
	{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
	},

	]);

//finding a single document
// Leaving the search criteria. empty will retrive all documents

// SELECT * FROM users; <<<----- SQL code
db.users.find();

// SELECT * from users WHERE firstname = "Stephen"; <<<----- SQL 
db.users.find({firstName: "Stephen"});

// The "pretty" method allows us to be able to view documents returned by our terminal
db.users.find({firstName: "Stephen"}).pretty();

// finding with multiple parameters

db.users.find({lastName: "Armstrong"}, {age: 82});


// updating single document

db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000000",
		email: "test@gmail.com"
	},
	courses: [""],
	department: "none"

});

/*
Syntax
	db.collectionName.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

// updating many documents

db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
);

// Replace One
/*
Syntax
	db.collectionName.replaceOne({criteria}, {$set: {field: value}});
*/

db.users.replaceOne(
	{ firstName: "Bill"},
	{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
	}
);

// [SECTION] Deleting Documents (delete)
db.users.insertOne({
	firstName: "test"
});

// Delete Many delete.Many / delete onbe delete.One
db.users.deleteMany({
	firstName: "test"
});


// [SECTION] Advanced Queries

db.users.find({
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	}
});

// Query on nested field

db.users.find({"contact.email":"stephenhawking@gmail.com"});

// Querying an Array without regardto order

db.users.find({courses:{$all: ["React", "Python"]}});

db.users.find({courses:{$all: ["React"]}});

// Querying and embeded Array]

db.users.insertOne({
	nameArray: [{nameA: "Juan"}, {nameB: "Tamad"}]
});
