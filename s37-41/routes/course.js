const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController.js')

const auth = require("../auth.js");


// Route for creating a course //always put auth.verify
router.post("/", auth.verify, (req, res) => {

	// const userData = auth.decode(req.headers.authorization)


	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});





// Get all courses

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});

// Get All Active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// retrieve specific course
router.get("/:courseId", (req, res) =>{
	console.log(req.params.courseId);
		courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
								//////req.params nasa URL
})


// Update a specific course
router.put("/:courseId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	courseController.updateCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
									//////req.params nasa URL

})


// archieve a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	courseController.archieveCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
									//////req.params nasa URL

})





module.exports = router;