

let http = require("http");

const port = 4000;

http.createServer(function(request, response){

	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to Booking System");
	}else if(request.url == "/profile"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to your profile!");
	}else if(request.url == "/courses"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Here’s our courses available");
	}else if(request.url == "/addcourse"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Add a course to our resource");
	}else if(request.url == "/updatecourse"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Update a course to our resources");
	}else if(request.url == "/archivecourse"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Archive courses to our resources");
	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("404: Page not found!");
	}
}).listen(port);

console.log(`Server running at localhost:${port}`);