// [SECTION] Comparison query

/*
	- Allows us to find documents that have field number values greater than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $gt : value } });
		db.collectionName.find({ field : { $gte : value } });
*/


// greater than
db.users.find({ age:{ $gt : 50 }});
db.users.find({ age:{ $gte : 50 }});

// less than


db.users.find({ age:{ $lt : 50 }});
db.users.find({ age:{ $lte : 50 }})

// $ne operator
/*
	- Allows us to find documents that have field number values not equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $ne : value } });
*/

db.users.find({ age:{ $ne : 50 }});

// $in operator
/*
	- Allows us to find documents with specific match critieria one field using different values.
	- Syntax
		db.collectionName.find({ field : { $in : value } });
*/
db.users.find({ lastName:{ $in : ["Hawking", "Doe"] }});
db.users.find({ courses:{ $in : ["HTML", "React"] }});


// [SECTION] Logical Query Operrator

// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provided search criteria.
	- Syntax
		db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/

db.users.find({ $or: [{firstName: "Neil"}, {age: 25}]});
db.users.find({ $or: [{firstName: "Neil"}, {age: {$gt: 30}}]});



// $and operator
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax
		db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/

db.users.find(
	{$and: 
		[
		{age: {$ne: 82}}, 
		{age: {$ne:76}}
		]
	}
);

// Inclusion
/*
	- Allows us to include/add specific fields only when retrieving documents.
	- The value provided is 1 to denote that the field is being included.
	- Syntax
		db.users.find({criteria},{field: 1})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{	
		firstName: 1,
		lastname: 1,
		contact: 1,
	}
);

// Exclusion

/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that the field is being included.
	- Syntax
		db.users.find({criteria},{field: 1})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{	
		contact: 0,
		department: 0
	}
);

// Suppressing ID Field
/*
	- Allows us to exclude the "_id" field when retrieving documents.
	- When using field projection, field inclusion and exclusion may not be used at the same time.
	- Excluding the "_id" field is the only exception to this rule.
	- Syntax
		db.users.find({criteria},{_id: 0})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{	
		firstName: 1,
		lastName: 1,
		"contact.phone" : 1
	}
);

// Suppressing specific fields in embedded document

db.users.find(
	{
		firstName: "Jane"
	},
	{	
		"contact.phone" : 0
	}
);

// Project specific array element in the returned array
// $slice operatyor allowes us to retrieve only 1 element that matches the search criteria

db.users.find(
	{"namearr" :
		{
			namea: "juan"
		}
	},
	{
		namearr: {$slice: 1}
	}
);

// [SECTION] Evaluation Query Operator

// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.
	- Syntax
		db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
*/

// Case sensitivity query
db.users.find({firstName: {$regex: "N"}});
db.users.find({firstName: {$regex: "n"}});

// Case insensitivity query
db.users.find({firstName: {$regex: "j", $options: "i"}});